package org.tof.syncdemo

import org.springframework.data.repository.CrudRepository

interface HomeRepository : CrudRepository<Home, String> {
}