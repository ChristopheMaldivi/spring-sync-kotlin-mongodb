package org.tof.syncdemo

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicInteger

internal object ParallelRequestsWithRxIoScheduler : ParallelRequests {

    private val log = LoggerFactory.getLogger(this.javaClass.simpleName)

    fun parallelRequestsRxIoScheduler(repo: HomeRepository, loopCount: Int) {
        val counter = AtomicInteger(0)
        val t0 = System.currentTimeMillis()

        repeat(loopCount) {
            Observable.just("test")
                    .observeOn(Schedulers.io())
                    .subscribe { doJob(repo, counter, loopCount, t0, log) }
        }
    }
}