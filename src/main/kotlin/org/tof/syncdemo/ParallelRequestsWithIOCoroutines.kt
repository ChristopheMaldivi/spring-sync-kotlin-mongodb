package org.tof.syncdemo

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicInteger

internal object ParallelRequestsWithIOCoroutines : ParallelRequests {

    private val log = LoggerFactory.getLogger(this.javaClass.simpleName)

    fun parallelRequestsIOCoroutines(repo: HomeRepository, loopCount: Int) {
        val counter = AtomicInteger(0)
        val t0 = System.currentTimeMillis()

        GlobalScope.launch(Dispatchers.IO) {
            repeat(loopCount) {
                async { doJob(repo, counter, loopCount, t0, log) }
            }
        }
    }

    fun requestIOCoroutine(repo: HomeRepository, id: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            async {
                log.info("get home for id $id")

                async {
                    Thread.sleep(100)
                    val home = repo.findById(Home.HOME_ID).get()
                    if (home.address.isNullOrEmpty()) {
                        throw IllegalStateException("bug")
                    }
                    log.info("got home for id $id")
                }
            }
        }
    }
}