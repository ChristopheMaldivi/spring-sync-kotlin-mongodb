package org.tof.syncdemo

import org.slf4j.Logger
import java.util.concurrent.atomic.AtomicInteger

interface ParallelRequests {

    fun doJob(repo: HomeRepository, counter: AtomicInteger, loopCount: Int, t0: Long, log: Logger) {
        val home = repo.findById(Home.HOME_ID).get()
        if (home.address.isNullOrEmpty()) {
            throw IllegalStateException("bug")
        }
        val count = counter.incrementAndGet()
//        log.info("count: $count")
        if (count >= loopCount) {
            log.info("duration: ${System.currentTimeMillis() - t0} ms")
        }
    }
}
