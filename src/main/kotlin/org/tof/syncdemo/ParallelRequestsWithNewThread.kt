package org.tof.syncdemo

import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread

internal object ParallelRequestsWithNewThread: ParallelRequests {

    private val log = LoggerFactory.getLogger(this.javaClass.simpleName)

    fun parallelRequestsNextThread(repo: HomeRepository, loopCount: Int) {
        val counter = AtomicInteger(0)
        val t0 = System.currentTimeMillis()

        repeat(loopCount) {
            thread(true) {
                doJob(repo, counter, loopCount, t0, log)
            }
        }
    }
}