package org.tof.syncdemo

import kotlinx.coroutines.*
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicInteger

internal object ParallelRequestsWithCoroutines : ParallelRequests {

    private val log = LoggerFactory.getLogger(this.javaClass.simpleName)

    fun parallelRequestsCoroutines(repo: HomeRepository, loopCount: Int) {
        val counter = AtomicInteger(0)
        val t0 = System.currentTimeMillis()

        GlobalScope.launch {
            repeat(loopCount) {
                async { doJob(repo, counter, loopCount, t0, log) }
            }
        }
    }
}