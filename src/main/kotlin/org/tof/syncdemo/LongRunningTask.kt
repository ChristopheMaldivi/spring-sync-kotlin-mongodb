package org.tof.syncdemo

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

object LongRunningTask {
    private val log = LoggerFactory.getLogger(this.javaClass.simpleName)

    fun doLongRunningBlockingTaskInBackground(id: Int) {
        Observable.just("test")
                .observeOn(Schedulers.io())
                .subscribe { doBlockingTask(id) }
    }

    fun doLongRunningAsyncTaskInBackground(id: Int) {
        Observable.just("test")
                .observeOn(Schedulers.computation())
                .subscribe { doAsyncTask(id) }
    }

    fun doLongRunningAsyncTaskInBackgroundCoroutine(id: Int) {
        GlobalScope.launch {
            repeat(5) {
                log.info("tick $id - $it")
                delay(1000)
            }
            log.info("done $id")
        }
    }

    private fun doBlockingTask(id: Int) {
        (0..4).forEach {
            log.info("tick $id - $it")
            Thread.sleep(1000)
        }
        log.info("done $id")
    }

    private fun doAsyncTask(id: Int) {
        recursiveLoop(id, AtomicInteger(0))
    }

    private fun recursiveLoop(id: Int, counter: AtomicInteger) {
        Observable.just(counter)
                .delay(1, TimeUnit.SECONDS)
                .observeOn(Schedulers.computation())
                .subscribe {
                    log.info("tick $id - ${counter.get()}")
                    if (counter.incrementAndGet() > 4) {
                        log.info("done $id")
                    } else {
                        recursiveLoop(id, counter)
                    }
                }
    }
}