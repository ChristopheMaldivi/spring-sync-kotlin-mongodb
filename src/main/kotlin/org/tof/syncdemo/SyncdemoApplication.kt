package org.tof.syncdemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SyncdemoApplication

fun main(args: Array<String>) {
	runApplication<SyncdemoApplication>(*args)
}
