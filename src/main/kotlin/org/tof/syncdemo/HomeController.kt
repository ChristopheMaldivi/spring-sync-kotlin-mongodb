package org.tof.syncdemo

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.tof.syncdemo.LongRunningTask.doLongRunningAsyncTaskInBackground
import org.tof.syncdemo.LongRunningTask.doLongRunningAsyncTaskInBackgroundCoroutine
import org.tof.syncdemo.LongRunningTask.doLongRunningBlockingTaskInBackground
import org.tof.syncdemo.ParallelRequestsWithIOCoroutines.requestIOCoroutine
import java.util.concurrent.atomic.AtomicInteger
import javax.annotation.PostConstruct

@RestController
class HomeController {
    companion object {
        private const val LOOP_COUNT = 3000
        private val log = LoggerFactory.getLogger(HomeController::class.java)
        private val counter = AtomicInteger(0)
    }

    @Autowired
    private lateinit var repo: HomeRepository

    @PostConstruct
    fun setup() {
        repo.deleteAll()
        repo.save(Home.example())

//        parallelRequestsNextThread(repo, LOOP_COUNT)
//        parallelRequestsRxComputationScheduler(repo, LOOP_COUNT)
//        parallelRequestsRxIoScheduler(repo, LOOP_COUNT)
//        parallelRequestsCoroutines(repo, LOOP_COUNT)
//        parallelRequestsIOCoroutines(repo, LOOP_COUNT)

        log.info("test started...")
    }

    @GetMapping("/")
    fun greeting() = "Hello spring kotlin ;)"

    @GetMapping("/home")
    fun home() = repo.findById(Home.HOME_ID).get()

    @GetMapping("/home-async-in-background")
    fun homeAsyncInBackground(): Int {
        val id = counter.incrementAndGet()
        requestIOCoroutine(repo, id)
//        log.info("return id $id")
        return id
    }

    @GetMapping("/long-running-blocking-task")
    fun longRunningBlockingTask(): Int {
        val id = counter.incrementAndGet()
        doLongRunningBlockingTaskInBackground(id)
        return id
    }

    @GetMapping("/long-running-async-task")
    fun longRunningAsyncTask(): Int {
        val id = counter.incrementAndGet()
        doLongRunningAsyncTaskInBackground(id)
        return id
    }

    @GetMapping("/long-running-async-task-coroutine")
    fun longRunningAsyncTaskCoroutine(): Int {
        val id = counter.incrementAndGet()
        doLongRunningAsyncTaskInBackgroundCoroutine(id)
        return id
    }
}

