package org.tof.syncdemo

data class ChatConfig(val authToken: String)

data class Buddy(
        val _id: String,
        val lovesRock: Boolean,
        val buddies: Array<String>,
        val chatConfig: ChatConfig,
        val avatar: String,
        val displayName: String,
        val nickName: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Buddy) return false

        if (_id != other._id) return false
        if (lovesRock != other.lovesRock) return false
        if (!buddies.contentEquals(other.buddies)) return false
        if (chatConfig != other.chatConfig) return false
        if (avatar != other.avatar) return false
        if (displayName != other.displayName) return false
        if (nickName != other.nickName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = _id.hashCode()
        result = 31 * result + lovesRock.hashCode()
        result = 31 * result + buddies.contentHashCode()
        result = 31 * result + chatConfig.hashCode()
        result = 31 * result + avatar.hashCode()
        result = 31 * result + displayName.hashCode()
        result = 31 * result + nickName.hashCode()
        return result
    }
}

data class Home(
        val _id: String,
        val buddies: Array<Buddy>,
        val homeName: String,
        val address: String,
        val op: String,
        val status: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Home) return false

        if (_id != other._id) return false
        if (!buddies.contentEquals(other.buddies)) return false
        if (homeName != other.homeName) return false
        if (address != other.address) return false
        if (op != other.op) return false
        if (status != other.status) return false

        return true
    }

    override fun hashCode(): Int {
        var result = _id.hashCode()
        result = 31 * result + buddies.contentHashCode()
        result = 31 * result + homeName.hashCode()
        result = 31 * result + address.hashCode()
        result = 31 * result + op.hashCode()
        result = 31 * result + status.hashCode()
        return result
    }

    companion object {
        const val HOME_ID = "5c8640d85eff7e001037d889"

        fun example(): Home {
            return Home(
                    HOME_ID,
                    arrayOf(
                            Buddy(
                                    "5c8640d85eff7e001037d88a",
                                    true,
                                    arrayOf(
                                            "eQKpoODtPX0cAPA91bE0M_FPgs1k186f_mGaIuinOVF8jU8TN4iUxdiDYCwXyao7T7spqHNwfdJidIoPg0zDq24v38HIc0_EHCoqMiSSNwWfrz5u3jB7HjzmjePD21iVHWtJaGrEiTLFuh49Y9iTa7A4",
                                            "crdEElmxFjIcAPA91bE6RTbVRbVPD9pmt_uW2ghoN1ZbYI8SO5vI5zQfBk4f5aRXf7KdsVXrUIjzRhNTTaeqmSu7cN1dVVXqtVjTaqu72rXwALJiDOHda5kMCrWI27GF_dg4pQkhCBfsGIpKyk5MJJMW"
                                    ),
                                    ChatConfig(
                                            "crdEElmxFjIcAPA91bE6RTbVRbVPD9pmt_uW2ghoN1ZbYI8SO5vI5zQfBk4f5aRXf7KdsVXrUIjzRhNTTaeqmSu7cN1dVVXqtVjTaqu72rXwALJiDOHda5kMCrWI27GF_dg4pQkhCBfsGIpKyk5MJJMW"
                                    ),
                                    "dogcat",
                                    "hahhah",
                                    "dfdlfdfdf9gCQCNSIQjHp9qhWw0fuY1wpuvGG9Y6cXXV7I4Fqyvc="
                            )
                    ),
                    "ddddddldkfdlkfjdlkfjdlfkjdflkj",
                    "tT21ItEkhtBhw4qBEw_iGhuAX2FzXsoNpxkYxa",
                    "obear",
                    "GOODTRIM"
            )
        }
    }
}
